const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
var {Todo} = require('./../server/models/todo');
var {User} = require('./../server/models/user');

var id = "5a1f8b894489ab9c513e0c1d";
var _id = "5a1f7aaa4698bf6e30745768"
if (!ObjectID.isValid(id)){
  console.log("Invalid id");
}

// Todo.find({
//   _id: id,
// }).then((todos) => {
//   console.log('Todos', todos);
// })
//
// Todo.findOne({
//   _id: id,
// }).then((todo) => {
//   console.log('Todo', todo);
// })

Todo.findById(id).then((todo) => {
  if(!todo){
    return console.log("id not found");
  }
  console.log("Todo by id:",todo);
}).catch((e) => console.log(e))

User.findById(_id).then((user) => {
  if(!user){
    return console.log("id not found");
  }
  console.log("User by id:", user);
}).catch((e) => console.log(e))
